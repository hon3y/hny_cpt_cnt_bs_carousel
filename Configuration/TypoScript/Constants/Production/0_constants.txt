plugin.tx_teufels_cpt_cnt_bs_carousel {
    view {
        templateRootPath = EXT:tx_teufels_cpt_cnt_bs_carousel/Resources/Private/Templates/
        partialRootPath = EXT:tx_teufels_cpt_cnt_bs_carousel/Resources/Private/Partials/
        layoutRootPath = EXT:tx_teufels_cpt_cnt_bs_carousel/Resources/Private/Layouts/
	}
    settings {
        production {
            includePath {
                public = EXT:teufels_cpt_cnt_bs_carousel/Resources/Public/
                private = EXT:teufels_cpt_cnt_bs_carousel/Resources/Private/
                frontend {
                    public = typo3conf/ext/teufels_cpt_cnt_bs_carousel/Resources/Public/
                }
            }
            bootstrapCarouselSimple {
                sClassOuter = carousel-outer carousel-fade slide lazyLazyCarousel
                sClassInner = carousel-inner
                sContainerElement = section
		        iInterval = 6000
            }
        }
    }
}